from flask import Flask, request, jsonify, render_template_string
import socket
import os
import requests
import setproctitle
from prometheus_client import start_http_server, Counter, Histogram
import time

# Setzen des Prozessnamens
setproctitle.setproctitle("sales")

app = Flask(__name__)

# Get environment variable(s)
SUFFIX = os.getenv('URL_SUFFIX', '')

ORDER_SERVICE_URL = f"http://order{SUFFIX}:8080/order/api"

# REST-Aufruf, um alle Bestellungen abzurufen
def get_orders():
    response = requests.get(ORDER_SERVICE_URL)
    if response.status_code == 200:
        return response.json()
    return []

# Funktion, um die Daten für die Tabellen aufzubereiten
def process_data(orders):
    products_total = {}
    customers_total = {}

    for order in orders:
        product_id = order['product_id']
        customer_id = order['customer_id']
        quantity = order['quantity']

        # Produktdetails vom order_service abrufen
        product_name, product_price = get_product_details(product_id)
        total_price = quantity * product_price

        # Produkte total berechnen
        if product_name not in products_total:
            products_total[product_name] = 0
        products_total[product_name] += total_price

        # Kundentotal berechnen
        customer_name = get_customer_name(customer_id)
        if customer_name not in customers_total:
            customers_total[customer_name] = 0
        customers_total[customer_name] += total_price

    return products_total, customers_total

# Helper functions to get product and customer details
def get_product_details(product_id):
    response = requests.get(f"http://catalog{SUFFIX}:8080/catalog/api/{product_id}")
    if response.status_code == 200:
        product = response.json()
        return product.get('name', 'Unknown Product'), product.get('price', 0)
    return 'Unknown Product', 0

def get_customer_name(customer_id):
    response = requests.get(f"http://customer{SUFFIX}:8080/customer/api/{customer_id}")
    if response.status_code == 200:
        return response.json().get('name', 'Unknown Customer')
    return 'Unknown Customer'

# Route zur Anzeige der HTML-Seite
@app.route('/sales')
def sales_page():
    orders = get_orders()
    products_total, customers_total = process_data(orders)

    html = """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
        <title>Sales Service</title>
    </head>
    <body>
        <div class="container">
            <h1 class="mt-5">Sales Report</h1>
            <h2 class="mt-5">Total per Product</h2>
            <table class="table table-striped mt-3">
                <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Total Sales</th>
                    </tr>
                </thead>
                <tbody>
                    {% for product, total in products_total.items() %}
                    <tr>
                        <td>{{ product }}</td>
                        <td>${{ total }}</td>
                    </tr>
                    {% endfor %}
                </tbody>
            </table>
            <h2 class="mt-5">Total per Customer</h2>
            <table class="table table-striped mt-3">
                <thead>
                    <tr>
                        <th>Customer Name</th>
                        <th>Total Sales</th>
                    </tr>
                </thead>
                <tbody>
                    {% for customer, total in customers_total.items() %}
                    <tr>
                        <td>{{ customer }}</td>
                        <td>${{ total }}</td>
                    </tr>
                    {% endfor %}
                </tbody>
            </table>
        </div>
    </body>
    </html>
    """
    return render_template_string(html, products_total=products_total, customers_total=customers_total)

@app.route('/metrics')
def metrics():
    from prometheus_client import generate_latest
    return generate_latest(), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
